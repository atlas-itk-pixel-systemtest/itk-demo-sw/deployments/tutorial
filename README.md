# Minimal tutorial stack
Minimal microservices stack to operate felix and optoboard.

[gitlab repository](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/deployments/tutorial)

## Getting started
This small guide assumes familiarity with docker and the optoboard / felix software and will only cover how to start and configure the microservices in this repository.

For a more general introduction into docker and the operation of the felix and optoboard microservices, see Benedikts [tutorial slides](https://indico.cern.ch/event/1409228/contributions/5923134/attachments/2843643/4971802/20240424_LLSQCminitutorial.pdf).

### Starting a microservice
Before starting execute the config_all.sh script to set the environment for all stacks.
To start a microservice simply change into the respective directory and execute:

```
docker compose up -d
```
Note: when using VSCode you can install the docker add-on and start the docker containers simply by right-clicking the compose files and chosing "Compose Up" in each directory.

The services have to be started in a (somewhat) specific order, as some of them depend on each other.

It is adviced to start with the backing services including:
- dockge (container management GUI)
- service-registry (container registration tool)
- rabbitmq (message queue needed for optoboard microservice)

And than start the felix & opto microservices in this order:
- felix microservice
- optoboard microservice

### Settings of the microservices
Some of the microservices (e.g. the felix ms) offer the option to configure their startup behaviour by changing their compose file. Information on the options for the felix ms can be found [here](https://demi.docs.cern.ch/itk-demo-felix/base-image/#environment-variables).

### Known issues
Note that the felix container by default will init, configure and start felixstar on startup. For now this behaviour is not propagted to the felix GUI correctly - meaning that the GUI will not reflect the correct felix state. If you want to operate the felix using the GUI it is adviced to turn of the automatic init, config and felixstar start or simply stop and start felixstar via the GUI once to get it into the right state.


# Extended tutorial stack
There is an extended tutorial stack including the configdb and the new (wip) dashboard available via the extended_tutorial branch of this repository.