# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
 
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
# .bashrc

# since we're not a real login shell...

export PS1="[DeMi] $(whoami)@$(hostname):$(pwd)\\$ "
export HOME=/config
export TERM="xterm-256color"
export SHELL=/bin/bash

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
PATH="/venv/bin:$PATH"
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
if [ -d ~/.bashrc.d ]; then
        for rc in ~/.bashrc.d/*; do
                if [ -f "$rc" ]; then
                        . "$rc"
                fi
        done
fi

unset rc

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
  
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

export VIRTUAL_ENV=/venv


# Export felix-release ENV variables
export FELIX_ROOT=/felix-05-00-04-rm5-stand-alone/x86_64-el9-gcc13-opt
export PATH=${FELIX_ROOT}/bin:/command:${PATH}
export LD_LIBRARY_PATH=${FELIX_ROOT}/lib:${LD_LIBRARY_PATH}


alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias ll='ls -l --color=auto'
alias vim=vi

alias checkFECerrorsMaster='flx-info link | grep "FEC error counters" -A30 | grep -e "  0 " -e "  4 " -e "  8 " -e " 12 " -e " 16 " -e " 20 "'
alias checkFECerrorsAll='flx-info link | grep "FEC error counters" -A30 | grep -e "  0 " -e "  2 " -e "  4 " -e "  6 " -e "  8 " -e " 10 " -e " 12 " -e " 14 " -e " 16 " -e " 18 " -e " 20 " -e " 22 "'
alias checkLinkAlignment='flx-info link | grep "Link alignment status" -A8 | grep -e "Channel" -e "Aligned"'

